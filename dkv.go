// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package dkv provides a simple on-disk key-value store.
package dkv

import (
	"compress/gzip"
	"encoding/hex"
	"errors"
	"hash/crc32"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
	"unicode/utf8"
)

// Store is a key-value store.
type Store struct {
	baseDir string
	m       sync.RWMutex
}

var (
	errEmptyKey       = errors.New("empty key")
	errCorruptedStore = errors.New("corrupted store")
	errDirNotEmpty    = errors.New("not empty")
	errNotADirectory  = errors.New("not a directory")
)

// New returns a new store.
func New(baseDir string) (*Store, error) {
	if baseDir == "" {
		baseDir = "dkv"
	}
	abspath, err := filepath.Abs(baseDir)
	if err != nil {
		return nil, err
	}
	return &Store{baseDir: abspath}, nil
}

// hexify returns fmt.Sprintf("%08x", v).
func hexify(v uint32) string {
	const hexdigit = "0123456789abcdef"
	b := make([]byte, 8)
	for i := 0; i < 8; i++ {
		b[7-i] = hexdigit[v%16]
		v /= 16
	}
	return string(b)
}

// fitKey returns a key of a suitable length, derived from s.
func fitKey(s string) string {
	if len(s) <= 71 {
		return s
	}
	cksum := crc32.ChecksumIEEE([]byte(s))
	s = s[:71-10]
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError {
		s = s[:len(s)-size]
	}
	return s + ".." + hexify(cksum)
}

// pathFor returns the complete path with the filename where the given key will be stored on the file system.
func (s *Store) pathFor(key string) string {
	cksum := crc32.ChecksumIEEE([]byte(key))
	hexStr := hexify(cksum)
	bkey := []byte(fitKey(key))
	return filepath.Join(s.baseDir, hexStr[:1], hexStr[1:3], hex.EncodeToString(bkey))
}

// Put writes the key-value pair to disk.
func (s *Store) Put(key string, val []byte) error {
	if s.baseDir == "" {
		panic("use of an uninitialized store")
	}
	if key == "" {
		return errEmptyKey
	}

	path := s.pathFor(key)

	s.m.Lock()
	defer s.m.Unlock()

	if err := os.MkdirAll(filepath.Dir(path), 0o700); err != nil {
		return err
	}

	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o600)
	if err != nil {
		return err
	}

	gzw := gzip.NewWriter(f)
	if _, err := gzw.Write(val); err != nil {
		f.Close()
		os.Remove(path)
		return err
	}

	if err := gzw.Close(); err != nil {
		f.Close()
		os.Remove(path)
		return err
	}

	if err := f.Close(); err != nil {
		os.Remove(path)
		return err
	}

	return nil
}

// Has returns true iff the given key exists.
func (s *Store) Has(key string) bool {
	if s.baseDir == "" {
		panic("use of an uninitialized store")
	}
	if key == "" {
		return false
	}

	path := s.pathFor(key)

	s.m.RLock()
	defer s.m.RUnlock()

	fi, err := os.Stat(path)
	if err != nil {
		return false
	}
	return fi.Mode().IsRegular()
}

// Get reads the value for the given key. On error it always returns a nil slice.
func (s *Store) Get(key string) ([]byte, error) {
	if s.baseDir == "" {
		panic("use of an uninitialized store")
	}
	if key == "" {
		return nil, errEmptyKey
	}

	path := s.pathFor(key)

	s.m.RLock()
	defer s.m.RUnlock()

	fi, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	if !fi.Mode().IsRegular() {
		return nil, errCorruptedStore
	}

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	gzr, err := gzip.NewReader(f)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(gzr)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// rmdir removes the directory if it is empty.
func rmdir(dir string) error {
	fi, err := os.Stat(dir)
	if err != nil {
		return err
	}
	if !fi.IsDir() {
		return errNotADirectory
	}

	rmerr := os.Remove(dir)
	if rmerr != nil {
		d, err := os.Open(dir)
		if err != nil {
			return rmerr
		}

		names, err := d.Readdirnames(1)
		d.Close()
		if err != nil && err != io.EOF {
			return rmerr
		}
		if len(names) > 0 {
			return errDirNotEmpty
		}
	}

	return rmerr
}

// removeEmptyDirs removes empty directories in path up to the base directory.
func (s *Store) removeEmptyDirs(path string) error {
	for i := 0; i < 2; i++ {
		path = filepath.Dir(path)
		if err := rmdir(path); err != nil {
			if err == errDirNotEmpty {
				break
			}
			return err
		}
	}
	return nil
}

// Remove removes the key from the store.
func (s *Store) Remove(key string) error {
	if s.baseDir == "" {
		panic("use of an uninitialized store")
	}
	if key == "" {
		return errEmptyKey
	}

	path := s.pathFor(key)

	s.m.Lock()
	defer s.m.Unlock()

	fi, err := os.Stat(path)
	if err != nil {
		return err
	}
	if !fi.Mode().IsRegular() {
		return errCorruptedStore
	}

	if err := os.Remove(path); err != nil {
		return err
	}

	return s.removeEmptyDirs(path)
}

// RemoveAll removes all the data from the store.
func (s *Store) RemoveAll() error {
	if s.baseDir == "" {
		panic("use of an uninitialized store")
	}
	s.m.Lock()
	defer s.m.Unlock()
	return os.RemoveAll(s.baseDir)
}
