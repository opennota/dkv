dkv [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/dkv?status.svg)](http://godoc.org/gitlab.com/opennota/dkv) [![Coverage report](https://gitlab.com/opennota/dkv/badges/master/coverage.svg)](https://gitlab.com/opennota/dkv/-/commits/master) [![Pipeline status](https://gitlab.com/opennota/dkv/badges/master/pipeline.svg)](https://gitlab.com/opennota/dkv/commits/master)
===

A simple on-disk key-value store.

## Install

    go get -u gitlab.com/opennota/dkv

