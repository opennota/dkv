// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package dkv

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"testing"
)

func TestHexify(t *testing.T) {
	const v = uint32(0x01234567)
	if want, got := fmt.Sprintf("%08x", v), hexify(v); got != want {
		t.Fatalf("hexify(%d): want %s, got %s", v, want, got)
	}
}

func TestHas(t *testing.T) {
	d, err := New("dkv")
	if err != nil {
		t.Fatalf("New: %v", err)
	}
	defer d.RemoveAll()

	k, v := "hello", []byte("world")
	if err := d.Put(k, v); err != nil {
		t.Fatal(err)
	}
	if has := d.Has(k); !has {
		t.Fatal("Has: want true, got false")
	}
	if has := d.Has("olleh"); has {
		t.Fatal("Has: want false, got true")
	}
}

func TestGetPut(t *testing.T) {
	d, err := New("dkv")
	if err != nil {
		t.Fatalf("New: %v", err)
	}
	defer d.RemoveAll()

	k, v := "hello", []byte("world")
	if err := d.Put(k, v); err != nil {
		t.Fatal(err)
	}
	if val, err := d.Get(k); err != nil {
		t.Fatal(err)
	} else if !bytes.Equal(val, v) {
		t.Fatalf("Get: want %v, got %v", v, val)
	}
	if _, err := d.Get("olleh"); !os.IsNotExist(err) {
		t.Fatalf("Get: want a doesn't exist error, got %v", err)
	}
}

func TestEmptyValue(t *testing.T) {
	d, err := New("dkv")
	if err != nil {
		t.Fatalf("New: %v", err)
	}
	defer d.RemoveAll()

	k, v := "empty", []byte(nil)
	if err := d.Put(k, v); err != nil {
		t.Fatal(err)
	}
	if val, err := d.Get(k); err != nil {
		t.Fatal(err)
	} else if len(val) > 0 || val == nil {
		t.Fatalf("Get: want an empty slice, got %v", val)
	}
}

func TestRemove(t *testing.T) {
	d, err := New("dkv")
	if err != nil {
		t.Fatalf("New: %v", err)
	}
	defer d.RemoveAll()

	k, v := "hello", []byte("world")
	if err := d.Put(k, v); err != nil {
		t.Fatal(err)
	}
	if val, err := d.Get(k); err != nil {
		t.Fatal(err)
	} else if !bytes.Equal(val, v) {
		t.Fatalf("Get: want %v, got %v", v, val)
	}

	if err := d.Remove(k); err != nil {
		t.Fatal(err)
	}
	if _, err := d.Get(k); !os.IsNotExist(err) {
		t.Fatalf("Get: want a doesn't exist error, got %v", err)
	}

	matches, err := filepath.Glob("dkv/*")
	if err != nil {
		t.Fatal(err)
	}
	if len(matches) > 0 {
		t.Fatalf("want no files left, got %v", matches)
	}
}

func TestRemoveAll(t *testing.T) {
	d, err := New("dkv")
	if err != nil {
		t.Fatalf("New: %v", err)
	}
	defer d.RemoveAll()

	k, v := "hello", []byte("world")
	if err := d.Put(k, v); err != nil {
		t.Fatal(err)
	}
	if val, err := d.Get(k); err != nil {
		t.Fatal(err)
	} else if !bytes.Equal(val, v) {
		t.Fatalf("Get: want %v, got %v", v, val)
	}

	if err := d.RemoveAll(); err != nil {
		t.Fatal(err)
	}
	if _, err := d.Get(k); !os.IsNotExist(err) {
		t.Fatalf("Get: want a doesn't exist error, got %v", err)
	}

	matches, err := filepath.Glob("dkv/*")
	if err != nil {
		t.Fatal(err)
	}
	if len(matches) > 0 {
		t.Fatalf("want no files left, got %v", matches)
	}
}
